import 'package:flutter/cupertino_1.dart';
import 'package:flutter/material.dart';
import 'package:mtn_app/app_page/Home_Sreen.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController phonenumberController = TextEditingController();
  TextEditingController createpasswordController = TextEditingController();
  TextEditingController reenterpasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration Page'),
      ),
      body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(children: <Widget>[
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: const Text('MTN APP ACADEMY',
                    style: TextStyle(
                        color: Colors.black54,
                        fontWeight: FontWeight.w500,
                        fontSize: 30))),
            Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Your Name',
                    ))),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                controller: surnameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Your Surname',
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                    controller: phonenumberController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Phone number',
                    ))),
            Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                    controller: createpasswordController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Create Password',
                    ))),
            Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                    controller: reenterpasswordController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Re-Enter Password',
                    ))),
            Container(
                height: 45,
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: RaisedButton(
                  textColor: Colors.black,
                  color: Colors.yellow,
                  child: Text('Register'),
                  onPressed: () {
                    print(nameController.text);
                    print(surnameController.text);
                    print(phonenumberController.text);
                    print(createpasswordController.text);
                    print(reenterpasswordController.text);

                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => const HomepageScreen()));
                  },
                )),
          ])),
    );
  }
}
