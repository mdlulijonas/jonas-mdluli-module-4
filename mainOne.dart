import "package:flutter/material.dart";
import 'app_page/Login_Screen.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'WELCOM TO MTN APP',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: AnimatedSplashScreen(
          splash: 'logo_MTN.png',
          nextScreen: const LoginPage(),
          splashTransition: SplashTransition.rotationTransition,
          duration: 3000,
        ));
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);
  @override
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 150,
              height: 150,
              child: Image.asset('logo_MTN.png'),
            ),
            Container(
              child: const Text(
                "Welcome ",
                style: const TextStyle(fontSize: 20),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
